﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form3 : Form
    {
        Bitmap btmap;
        Graphics g;
        public Form3()
        {
            InitializeComponent(); 
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            this.btmap = new Bitmap(640, 480);
            this.g = Graphics.FromImage(this.btmap);
        }
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            
            Pen p = new Pen(Color.Red, 2);
            g.DrawLine(p, 0, 0, 640, 480);

            Graphics windowG = e.Graphics;
            windowG.DrawImageUnscaled(btmap, 0, 0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            btmap.Save("./image.jpg", ImageFormat.Jpeg);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = new Bitmap("./image.jpg");
        }

    }
}
