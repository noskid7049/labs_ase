﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Point pt1 = new Point(0, 0);
            Point pt2 = new Point(pictureBox1.Size);

            Rectangle rct1 = new Rectangle(50, 80, 100, 130);
            Graphics g = e.Graphics;
            Pen bckPen = new Pen(Color.Black, 5);

            g.DrawLine(bckPen, pt1, pt2);
            g.DrawLine(bckPen, 0, 50, 200, 50);

            g.DrawEllipse(bckPen, 50, 50, 200, 100);

            g.DrawRectangle(bckPen, rct1);
            bckPen.Dispose();
        }
    }
}
