﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void submitBtn_Click(object sender, EventArgs e)
        {
            string fname,lname, email, regNum;
            fname= fnameBox.Text;
            lname = lnameBox.Text;
            email = emailBox.Text;
            regNum = regNumberBox.Text;

            MessageBox.Show($"Hello {fname} {lname} ({regNum}), your email is {email}");
        }
    }
}
