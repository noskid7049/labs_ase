﻿namespace Calculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.num1 = new System.Windows.Forms.Button();
            this.num2 = new System.Windows.Forms.Button();
            this.num3 = new System.Windows.Forms.Button();
            this.symDiv = new System.Windows.Forms.Button();
            this.symMul = new System.Windows.Forms.Button();
            this.num6 = new System.Windows.Forms.Button();
            this.num5 = new System.Windows.Forms.Button();
            this.num4 = new System.Windows.Forms.Button();
            this.symMin = new System.Windows.Forms.Button();
            this.num9 = new System.Windows.Forms.Button();
            this.num8 = new System.Windows.Forms.Button();
            this.num7 = new System.Windows.Forms.Button();
            this.symEqual = new System.Windows.Forms.Button();
            this.symDot = new System.Windows.Forms.Button();
            this.num0 = new System.Windows.Forms.Button();
            this.symPlus = new System.Windows.Forms.Button();
            this.outputBox = new System.Windows.Forms.TextBox();
            this.resultBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // num1
            // 
            this.num1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num1.Location = new System.Drawing.Point(12, 293);
            this.num1.Name = "num1";
            this.num1.Size = new System.Drawing.Size(66, 54);
            this.num1.TabIndex = 1;
            this.num1.Text = "1";
            this.num1.UseVisualStyleBackColor = true;
            this.num1.Click += new System.EventHandler(this.num_Click);
            // 
            // num2
            // 
            this.num2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num2.Location = new System.Drawing.Point(93, 293);
            this.num2.Name = "num2";
            this.num2.Size = new System.Drawing.Size(66, 54);
            this.num2.TabIndex = 2;
            this.num2.Text = "2";
            this.num2.UseVisualStyleBackColor = true;
            this.num2.Click += new System.EventHandler(this.num_Click);
            // 
            // num3
            // 
            this.num3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num3.Location = new System.Drawing.Point(177, 293);
            this.num3.Name = "num3";
            this.num3.Size = new System.Drawing.Size(66, 54);
            this.num3.TabIndex = 3;
            this.num3.Text = "3";
            this.num3.UseVisualStyleBackColor = true;
            this.num3.Click += new System.EventHandler(this.num_Click);
            // 
            // symDiv
            // 
            this.symDiv.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.symDiv.Location = new System.Drawing.Point(260, 137);
            this.symDiv.Name = "symDiv";
            this.symDiv.Size = new System.Drawing.Size(66, 54);
            this.symDiv.TabIndex = 4;
            this.symDiv.Text = "/";
            this.symDiv.UseVisualStyleBackColor = true;
            this.symDiv.Click += new System.EventHandler(this.sym_Click);
            // 
            // symMul
            // 
            this.symMul.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.symMul.Location = new System.Drawing.Point(260, 213);
            this.symMul.Name = "symMul";
            this.symMul.Size = new System.Drawing.Size(66, 54);
            this.symMul.TabIndex = 8;
            this.symMul.Text = "X";
            this.symMul.UseVisualStyleBackColor = true;
            this.symMul.Click += new System.EventHandler(this.sym_Click);
            // 
            // num6
            // 
            this.num6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num6.Location = new System.Drawing.Point(177, 213);
            this.num6.Name = "num6";
            this.num6.Size = new System.Drawing.Size(66, 54);
            this.num6.TabIndex = 7;
            this.num6.Text = "6";
            this.num6.UseVisualStyleBackColor = true;
            this.num6.Click += new System.EventHandler(this.num_Click);
            // 
            // num5
            // 
            this.num5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num5.Location = new System.Drawing.Point(93, 213);
            this.num5.Name = "num5";
            this.num5.Size = new System.Drawing.Size(66, 54);
            this.num5.TabIndex = 6;
            this.num5.Text = "5";
            this.num5.UseVisualStyleBackColor = true;
            this.num5.Click += new System.EventHandler(this.num_Click);
            // 
            // num4
            // 
            this.num4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num4.Location = new System.Drawing.Point(12, 213);
            this.num4.Name = "num4";
            this.num4.Size = new System.Drawing.Size(66, 54);
            this.num4.TabIndex = 5;
            this.num4.Text = "4";
            this.num4.UseVisualStyleBackColor = true;
            this.num4.Click += new System.EventHandler(this.num_Click);
            // 
            // symMin
            // 
            this.symMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.symMin.Location = new System.Drawing.Point(260, 293);
            this.symMin.Name = "symMin";
            this.symMin.Size = new System.Drawing.Size(66, 54);
            this.symMin.TabIndex = 12;
            this.symMin.Text = "-";
            this.symMin.UseVisualStyleBackColor = true;
            this.symMin.Click += new System.EventHandler(this.sym_Click);
            // 
            // num9
            // 
            this.num9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num9.Location = new System.Drawing.Point(177, 137);
            this.num9.Name = "num9";
            this.num9.Size = new System.Drawing.Size(66, 54);
            this.num9.TabIndex = 11;
            this.num9.Text = "9";
            this.num9.UseVisualStyleBackColor = true;
            this.num9.Click += new System.EventHandler(this.num_Click);
            // 
            // num8
            // 
            this.num8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num8.Location = new System.Drawing.Point(93, 137);
            this.num8.Name = "num8";
            this.num8.Size = new System.Drawing.Size(66, 54);
            this.num8.TabIndex = 10;
            this.num8.Text = "8";
            this.num8.UseVisualStyleBackColor = true;
            this.num8.Click += new System.EventHandler(this.num_Click);
            // 
            // num7
            // 
            this.num7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num7.Location = new System.Drawing.Point(12, 137);
            this.num7.Name = "num7";
            this.num7.Size = new System.Drawing.Size(66, 54);
            this.num7.TabIndex = 9;
            this.num7.Text = "7";
            this.num7.UseVisualStyleBackColor = true;
            this.num7.Click += new System.EventHandler(this.num_Click);
            // 
            // symEqual
            // 
            this.symEqual.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.symEqual.Location = new System.Drawing.Point(260, 371);
            this.symEqual.Name = "symEqual";
            this.symEqual.Size = new System.Drawing.Size(66, 54);
            this.symEqual.TabIndex = 16;
            this.symEqual.Text = "=";
            this.symEqual.UseVisualStyleBackColor = true;
            this.symEqual.Click += new System.EventHandler(this.symEqual_Click);
            // 
            // symDot
            // 
            this.symDot.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.symDot.Location = new System.Drawing.Point(177, 371);
            this.symDot.Name = "symDot";
            this.symDot.Size = new System.Drawing.Size(66, 54);
            this.symDot.TabIndex = 15;
            this.symDot.Text = ".";
            this.symDot.UseVisualStyleBackColor = true;
            this.symDot.Click += new System.EventHandler(this.symDot_Click);
            // 
            // num0
            // 
            this.num0.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num0.Location = new System.Drawing.Point(93, 371);
            this.num0.Name = "num0";
            this.num0.Size = new System.Drawing.Size(66, 54);
            this.num0.TabIndex = 14;
            this.num0.Text = "0";
            this.num0.UseVisualStyleBackColor = true;
            this.num0.Click += new System.EventHandler(this.num_Click);
            // 
            // symPlus
            // 
            this.symPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.symPlus.Location = new System.Drawing.Point(12, 371);
            this.symPlus.Name = "symPlus";
            this.symPlus.Size = new System.Drawing.Size(66, 54);
            this.symPlus.TabIndex = 13;
            this.symPlus.Text = "+";
            this.symPlus.UseVisualStyleBackColor = true;
            this.symPlus.Click += new System.EventHandler(this.sym_Click);
            // 
            // outputBox
            // 
            this.outputBox.Location = new System.Drawing.Point(12, 12);
            this.outputBox.Multiline = true;
            this.outputBox.Name = "outputBox";
            this.outputBox.ReadOnly = true;
            this.outputBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.outputBox.Size = new System.Drawing.Size(314, 66);
            this.outputBox.TabIndex = 0;
            // 
            // resultBox
            // 
            this.resultBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.resultBox.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.resultBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultBox.Location = new System.Drawing.Point(23, 33);
            this.resultBox.Multiline = true;
            this.resultBox.Name = "resultBox";
            this.resultBox.ReadOnly = true;
            this.resultBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.resultBox.Size = new System.Drawing.Size(293, 35);
            this.resultBox.TabIndex = 17;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 450);
            this.Controls.Add(this.resultBox);
            this.Controls.Add(this.symEqual);
            this.Controls.Add(this.symDot);
            this.Controls.Add(this.num0);
            this.Controls.Add(this.symPlus);
            this.Controls.Add(this.symMin);
            this.Controls.Add(this.num9);
            this.Controls.Add(this.num8);
            this.Controls.Add(this.num7);
            this.Controls.Add(this.symMul);
            this.Controls.Add(this.num6);
            this.Controls.Add(this.num5);
            this.Controls.Add(this.num4);
            this.Controls.Add(this.symDiv);
            this.Controls.Add(this.num3);
            this.Controls.Add(this.num2);
            this.Controls.Add(this.num1);
            this.Controls.Add(this.outputBox);
            this.Name = "Form1";
            this.Text = "Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button num1;
        private System.Windows.Forms.Button num2;
        private System.Windows.Forms.Button num3;
        private System.Windows.Forms.Button symDiv;
        private System.Windows.Forms.Button symMul;
        private System.Windows.Forms.Button num6;
        private System.Windows.Forms.Button num5;
        private System.Windows.Forms.Button num4;
        private System.Windows.Forms.Button symMin;
        private System.Windows.Forms.Button num9;
        private System.Windows.Forms.Button num8;
        private System.Windows.Forms.Button num7;
        private System.Windows.Forms.Button symEqual;
        private System.Windows.Forms.Button symDot;
        private System.Windows.Forms.Button num0;
        private System.Windows.Forms.Button symPlus;
        private System.Windows.Forms.TextBox outputBox;
        private System.Windows.Forms.TextBox resultBox;
    }
}

