﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        string displayStr;
        float calcNum, output;
        string currentSym;
        bool firstNum;
        public Form1()
        {
            InitializeComponent();
            this.displayStr = "";
            this.currentSym = "+";
            this.calcNum = 0;
            this.output = 0;
            this.firstNum = true;
        }
        private void num_Click(object sender, EventArgs e)
        {
            // Get the number of the cliecked btn
            string txt = ((Button)sender).Text;
            // Add the number to the display and calculate in the final output
            addToDisplayStr(txt);
            calcCalcNum(Convert.ToInt32(txt));

        }
        private void symDot_Click(object sender, EventArgs e)
        {
            addToDisplayStr(".");
            this.currentSym = ".";
        }

        //Symbol clicks

        private void symEqual_Click(object sender, EventArgs e)
        {
            calcToOutput();
            this.resultBox.Text = output.ToString();
            // Reset all the variables
            this.displayStr = "";
            this.currentSym = "+";
            this.calcNum = 0;
            this.output = 0;
            this.firstNum = true;
        }
        private void sym_Click(object sender, EventArgs e)
        {
            // Get the symbol of the clicked button
            string sym = ((Button)sender).Text;
            calcToOutput();
            // Convert the display X to * for calculation
            if (sym == "X") sym = "*";
            this.currentSym = sym;
            addToDisplayStr(currentSym);
        }

        // Helpers

        private void addToDisplayStr(string str)
        {
            this.displayStr += str;
            this.outputBox.Text = displayStr;
        }

        private void calcCalcNum(int num)
        {
            // Check if the current sym is dot
            // if yes, add the num to the end of decimal places
            if (currentSym != ".")
            {
                calcNum = calcNum * 10 + num;
            }
            else
            {
                string temp = calcNum.ToString();
                // Check if the calcNum has decimals
                // if no decimal, forcefully add . to the end of the string
                if (calcNum == Math.Floor(calcNum))
                {
                    temp += ".";
                }
                temp += num.ToString();
                calcNum = float.Parse(temp);
            }
            resultBox.Text = calcNum.ToString();
            if (firstNum) output = calcNum;
        }

        private void calcToOutput()
        {
            // If the current num is not the first number, do calculations
            if (!this.firstNum)
            {
                switch (this.currentSym)
                {
                    case "+":
                        this.output += this.calcNum;
                        break;
                    case "-":
                        this.output -= this.calcNum;
                        // code block
                        break;
                    case "/":
                        this.output /= this.calcNum;
                        // code block
                        break;
                    case "*":
                        this.output *= this.calcNum;
                        // code block
                        break;
                    default:
                        // code block
                        break;
                }
            }
            // Change the text of the result box to the output
            this.resultBox.Text = this.output.ToString();
            if (this.firstNum) { this.firstNum = false; }
            // Reset the current calcNum to 0
            this.calcNum = 0;
        }
    }
}
