﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab2
{
    // ENUM for shape
    enum Shape
    {
        SQUARE = 1,
        RECTANGLE,
        ECLIPSE
    }
    public partial class Form1 : Form
    {
        Graphics g;

        // Set initial shape to 0;
        int reqShape = 0;

        // Nullable int for X and Y
        int? initX = null;
        int? initY = null;

        // Pen width and size of the shape
        int penWidth = 0;
        float size = 0;
        // Boolean for start painting
        bool startPaint = false;
        public Form1()
        {
            InitializeComponent();
            // Get the graphics of the panel where drawing will be done
            this.g = drawPanel.CreateGraphics();
        }

        private void setPenBtn_Click(object sender, EventArgs e)
        {
            // Create a new color dialog box and show the color dialog box
            ColorDialog c = new ColorDialog();
            if (c.ShowDialog() == DialogResult.OK)
            {
                //Set the back color of the pen color btn to the color selected in the color dialog box
                this.penColorBtn.BackColor = c.Color;
            }
        }

        private void setCanvasBtn_Click(object sender, EventArgs e)
        {
            // Create a new color dialog box and show the color dialog box
            ColorDialog c = new ColorDialog();
            if (c.ShowDialog() == DialogResult.OK)
            {
                //Set the back color of the canvas color btn to the color selected in the color dialog box
                this.setCanvasBtn.BackColor = c.Color;
                this.drawPanel.BackColor = c.Color;
            }
        }

        private void penWidthCmbBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Set the penWidth every time the selected option is changed
            this.penWidth = Convert.ToInt32(this.penWidthCmbBox.SelectedItem.ToString());
        }

        private void sizeTextBox_TextChanged(object sender, EventArgs e)
        {
            // Convert the text in the txt box to float for storing
            this.size = float.Parse(this.sizeTextBox.Text);
        }

        private void drawPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (startPaint)
            {
                // Create a new pen using the color selected and the given pen width
                Pen p = new Pen(this.penColorBtn.BackColor, (float)this.penWidth);
                //Draw the line on drag
                Point oldPoint = new Point(initX ?? e.X, initY ?? e.Y);
                Point newPoint = new Point(e.X, e.Y);
                this.g.DrawLine(p, oldPoint, newPoint);
                initX = e.X;
                initY = e.Y;
            }
        }

        private void drawPanel_MouseDown(object sender, MouseEventArgs e)
        {
            this.startPaint = true;
            if (this.reqShape == (int)Shape.SQUARE)
            {
                // Create a solid brush with the required color
                SolidBrush sb = new SolidBrush(this.penColorBtn.BackColor);
                // Set the same width and height for square
                this.g.FillRectangle(sb, e.X, e.Y, this.size, this.size);
            }
            else if (this.reqShape == (int)Shape.RECTANGLE)
            {
                // Create a solid brush with the required color
                SolidBrush sb = new SolidBrush(this.penColorBtn.BackColor);
                // Set the width and height(width *2) for square
                this.g.FillRectangle(sb, e.X, e.Y, this.size, this.size * 2);
            }
            else if (this.reqShape == (int)Shape.ECLIPSE)
            {
                // Create a solid brush with the required color
                SolidBrush sb = new SolidBrush(this.penColorBtn.BackColor);
                //Draw eclipse
                this.g.FillEllipse(sb, e.X, e.Y, this.size, this.size);
            }
            //Reset the start paint bool if any shape is selected
            if (this.reqShape > 0) this.startPaint = false;
            this.reqShape = 0;
        }

        private void squareBtn_Click(object sender, EventArgs e)
        {
            // Set the required shape to SQUARE
            this.reqShape = (int)Shape.SQUARE;
        }

        private void rectBtn_Click(object sender, EventArgs e)
        {
            // Set the required shape to RECTANGLE
            this.reqShape = (int)Shape.RECTANGLE;
        }

        private void eclipseBtn_Click(object sender, EventArgs e)
        {
            // Set the required shape to ECLIPSE
            this.reqShape = (int)Shape.ECLIPSE;
        }

        private void drawPanel_MouseUp(object sender, MouseEventArgs e)
        {
            // Reset the start paint bool and the X and Y
            this.startPaint = false;
            this.initX = null;
            this.initY = null;
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Clear the panel on clear btn click
            this.g.Clear(Color.White);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Exit the application
            Application.Exit();
        }
    }
}
