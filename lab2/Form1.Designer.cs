﻿namespace lab2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.setPenBtn = new System.Windows.Forms.Button();
            this.penColorBtn = new System.Windows.Forms.Button();
            this.setCanvasBtn = new System.Windows.Forms.Button();
            this.squareBtn = new System.Windows.Forms.Button();
            this.rectBtn = new System.Windows.Forms.Button();
            this.eclipseBtn = new System.Windows.Forms.Button();
            this.penWidthCmbBox = new System.Windows.Forms.ComboBox();
            this.penWidthLabel = new System.Windows.Forms.Label();
            this.sizeLabel = new System.Windows.Forms.Label();
            this.sizeTextBox = new System.Windows.Forms.TextBox();
            this.drawPanel = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.homeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // setPenBtn
            // 
            this.setPenBtn.BackColor = System.Drawing.SystemColors.Control;
            this.setPenBtn.Location = new System.Drawing.Point(44, 31);
            this.setPenBtn.Name = "setPenBtn";
            this.setPenBtn.Size = new System.Drawing.Size(75, 23);
            this.setPenBtn.TabIndex = 0;
            this.setPenBtn.Text = "Set Pen";
            this.setPenBtn.UseVisualStyleBackColor = false;
            this.setPenBtn.Click += new System.EventHandler(this.setPenBtn_Click);
            // 
            // penColorBtn
            // 
            this.penColorBtn.BackColor = System.Drawing.SystemColors.Control;
            this.penColorBtn.Location = new System.Drawing.Point(185, 31);
            this.penColorBtn.Name = "penColorBtn";
            this.penColorBtn.Size = new System.Drawing.Size(75, 23);
            this.penColorBtn.TabIndex = 1;
            this.penColorBtn.Text = "Pen Color";
            this.penColorBtn.UseVisualStyleBackColor = false;
            // 
            // setCanvasBtn
            // 
            this.setCanvasBtn.BackColor = System.Drawing.SystemColors.Control;
            this.setCanvasBtn.Location = new System.Drawing.Point(358, 31);
            this.setCanvasBtn.Name = "setCanvasBtn";
            this.setCanvasBtn.Size = new System.Drawing.Size(75, 23);
            this.setCanvasBtn.TabIndex = 2;
            this.setCanvasBtn.Text = "Set Canvas";
            this.setCanvasBtn.UseVisualStyleBackColor = false;
            this.setCanvasBtn.Click += new System.EventHandler(this.setCanvasBtn_Click);
            // 
            // squareBtn
            // 
            this.squareBtn.BackColor = System.Drawing.SystemColors.Control;
            this.squareBtn.Location = new System.Drawing.Point(44, 83);
            this.squareBtn.Name = "squareBtn";
            this.squareBtn.Size = new System.Drawing.Size(75, 23);
            this.squareBtn.TabIndex = 3;
            this.squareBtn.Text = "Square";
            this.squareBtn.UseVisualStyleBackColor = false;
            this.squareBtn.Click += new System.EventHandler(this.squareBtn_Click);
            // 
            // rectBtn
            // 
            this.rectBtn.BackColor = System.Drawing.SystemColors.Control;
            this.rectBtn.Location = new System.Drawing.Point(185, 83);
            this.rectBtn.Name = "rectBtn";
            this.rectBtn.Size = new System.Drawing.Size(75, 23);
            this.rectBtn.TabIndex = 4;
            this.rectBtn.Text = "Rectangle";
            this.rectBtn.UseVisualStyleBackColor = false;
            this.rectBtn.Click += new System.EventHandler(this.rectBtn_Click);
            // 
            // eclipseBtn
            // 
            this.eclipseBtn.BackColor = System.Drawing.SystemColors.Control;
            this.eclipseBtn.Location = new System.Drawing.Point(358, 83);
            this.eclipseBtn.Name = "eclipseBtn";
            this.eclipseBtn.Size = new System.Drawing.Size(75, 23);
            this.eclipseBtn.TabIndex = 5;
            this.eclipseBtn.Text = "Eclipse";
            this.eclipseBtn.UseVisualStyleBackColor = false;
            this.eclipseBtn.Click += new System.EventHandler(this.eclipseBtn_Click);
            // 
            // penWidthCmbBox
            // 
            this.penWidthCmbBox.FormattingEnabled = true;
            this.penWidthCmbBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.penWidthCmbBox.Location = new System.Drawing.Point(615, 33);
            this.penWidthCmbBox.Name = "penWidthCmbBox";
            this.penWidthCmbBox.Size = new System.Drawing.Size(121, 21);
            this.penWidthCmbBox.TabIndex = 6;
            this.penWidthCmbBox.SelectedIndexChanged += new System.EventHandler(this.penWidthCmbBox_SelectedIndexChanged);
            // 
            // penWidthLabel
            // 
            this.penWidthLabel.AutoSize = true;
            this.penWidthLabel.Location = new System.Drawing.Point(534, 36);
            this.penWidthLabel.Name = "penWidthLabel";
            this.penWidthLabel.Size = new System.Drawing.Size(57, 13);
            this.penWidthLabel.TabIndex = 7;
            this.penWidthLabel.Text = "Pen Width";
            // 
            // sizeLabel
            // 
            this.sizeLabel.AutoSize = true;
            this.sizeLabel.Location = new System.Drawing.Point(564, 93);
            this.sizeLabel.Name = "sizeLabel";
            this.sizeLabel.Size = new System.Drawing.Size(27, 13);
            this.sizeLabel.TabIndex = 8;
            this.sizeLabel.Text = "Size";
            // 
            // sizeTextBox
            // 
            this.sizeTextBox.Location = new System.Drawing.Point(615, 93);
            this.sizeTextBox.Name = "sizeTextBox";
            this.sizeTextBox.Size = new System.Drawing.Size(121, 20);
            this.sizeTextBox.TabIndex = 9;
            this.sizeTextBox.TextChanged += new System.EventHandler(this.sizeTextBox_TextChanged);
            // 
            // drawPanel
            // 
            this.drawPanel.BackColor = System.Drawing.SystemColors.Control;
            this.drawPanel.Location = new System.Drawing.Point(44, 137);
            this.drawPanel.Name = "drawPanel";
            this.drawPanel.Size = new System.Drawing.Size(692, 301);
            this.drawPanel.TabIndex = 10;
            this.drawPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.drawPanel_MouseDown);
            this.drawPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.drawPanel_MouseMove);
            this.drawPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.drawPanel_MouseUp);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.homeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // homeToolStripMenuItem
            // 
            this.homeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.homeToolStripMenuItem.Name = "homeToolStripMenuItem";
            this.homeToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.homeToolStripMenuItem.Text = "Home";
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.clearToolStripMenuItem.Text = "Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.drawPanel);
            this.Controls.Add(this.sizeTextBox);
            this.Controls.Add(this.sizeLabel);
            this.Controls.Add(this.penWidthLabel);
            this.Controls.Add(this.penWidthCmbBox);
            this.Controls.Add(this.eclipseBtn);
            this.Controls.Add(this.rectBtn);
            this.Controls.Add(this.squareBtn);
            this.Controls.Add(this.setCanvasBtn);
            this.Controls.Add(this.penColorBtn);
            this.Controls.Add(this.setPenBtn);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button setPenBtn;
        private System.Windows.Forms.Button penColorBtn;
        private System.Windows.Forms.Button setCanvasBtn;
        private System.Windows.Forms.Button squareBtn;
        private System.Windows.Forms.Button rectBtn;
        private System.Windows.Forms.Button eclipseBtn;
        private System.Windows.Forms.ComboBox penWidthCmbBox;
        private System.Windows.Forms.Label penWidthLabel;
        private System.Windows.Forms.Label sizeLabel;
        private System.Windows.Forms.TextBox sizeTextBox;
        private System.Windows.Forms.Panel drawPanel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem homeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}

